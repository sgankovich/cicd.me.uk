provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  region     = var.aws_region
}

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "cicd-me-uk"
    workspaces {
      name = "cicdmeuk"
    }
  }
}

resource "aws_vpc" "vpc" {
  enable_dns_support   = true
  enable_dns_hostnames = true
  cidr_block           = var.vpc_cidr
  tags                 = var.default_tags
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags   = var.default_tags
}

resource "aws_subnet" "subnet_public_a" {
  vpc_id                  = aws_vpc.vpc.id
  map_public_ip_on_launch = "true"
  cidr_block              = var.cidr_subnet_public_a
  availability_zone       = var.availability_zone_a
  tags                    = var.default_tags
}

resource "aws_route_table" "rtbl" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = var.default_tags
}

resource "aws_route_table_association" "rta" {
  subnet_id      = aws_subnet.subnet_public_a.id
  route_table_id = aws_route_table.rtbl.id
}

resource "aws_security_group" "sg_inst" {
  name   = "sg_inst"
  vpc_id = aws_vpc.vpc.id
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = var.default_tags
}

resource "aws_instance" "instance" {
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  key_name               = var.ssh_key_name
  subnet_id              = aws_subnet.subnet_public_a.id
  vpc_security_group_ids = [aws_security_group.sg_inst.id]

  provisioner "remote-exec" {
    inline = [
      "echo 1"
    ]
    connection {
      type        = "ssh"
      user        = var.username
      private_key = file(var.ssh_key_path)
      host        = self.public_ip
    }
  }
  tags = var.default_tags
}

resource "null_resource" "generatehostsfile" {
    triggers = {
        build_number = timestamp()
    }
    provisioner "local-exec" {
    command = <<EOT
      echo "[${var.host_label}:vars]" > hosts
      echo "ansible_ssh_user=${var.username}" >> hosts
      echo "ansible_ssh_private_key_file=${var.ssh_key_path}" >> hosts
      echo "ansible_ssh_common_args='-o StrictHostKeyChecking=no'" >> hosts 
      echo "[${var.host_label}]" >> hosts
      echo ${aws_instance.instance.public_ip} >> hosts
    EOT
  }
  depends_on = [aws_instance.instance]
}

data "aws_route53_zone" "primary" {
  name = var.dnsname
}
#Creating aws_route_53
resource "aws_route53_record" "dns_record_name" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = data.aws_route53_zone.primary.name
  type    = "A"
  ttl     = "300"
  records = [aws_instance.instance.public_ip]
  depends_on = [aws_instance.instance]
}

resource "aws_route53_record" "dns_record_www" {
  zone_id = data.aws_route53_zone.primary.zone_id
  name    = "www.${data.aws_route53_zone.primary.name}"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.instance.public_ip]
  depends_on = [aws_instance.instance]
}
