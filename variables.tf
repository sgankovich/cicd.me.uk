variable "aws_access_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}

variable "username" {
  type    = string
  default = "ubuntu"
}
variable "ssh_key_name" {
  type    = string
  default = "rgsx_key"
}

variable "host_label" {
  type    = string
  default = "ubuntu"
}

variable "ssh_key_path" {
  type    = string
  default = "rgsx_key.pem"
}

variable "dnsname" {
  type    = string
  default = "cicd.me.uk"
}
variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

variable "availability_zone_a" {
  type    = string
  default = "eu-central-1a"
}

variable "instance_ami" {
  type    = string
  default = "ami-0d359437d1756caa8"
}

variable "instance_type" {
  type    = string
  default = "t2.nano"
}

variable "vpc_cidr" {
  type    = string
  default = "10.0.0.0/16"
}

variable "cidr_subnet_public_a" {
  type    = string
  default = "10.0.1.0/24"
}

variable "environment_tag" {
  type    = string
  default = "cicdmeuk"
}

variable "default_tags" {
  description = "The set of tags."
  type        = map
  default = {
    "Environment" = "dev",
    "Project"     = "cicdmeuk",
  }
}
